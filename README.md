# About

This library gives you a new way for writing queries. Using the [Specification pattern](https://en.wikipedia.org/wiki/Specification_pattern) you will
get small Specification classes that are highly reusable.

## Why should we use this library?

You are probably wondering why we created this library. Your entity repositories are working just fine as they are, right?

But if your friend open one of your repository classes he/she would probably find that the code is not as perfect as you thought.
Entity repositories have a tendency to get messy. Problems may include:

 * Unable to reuse criteria
 * Some functions have too many arguments
 * Code duplication
 * Difficult to test

## Advantages

The solution should have the following features:

* Easy to test
* Easy to extend, store and run
* Re-usable code
* Single responsibility principle
* Hides the implementation details of the ORM. (This might seen like nitpicking, however it leads to bloated client code
doing the query builder work over and over again.)

# Installation

## Add repository to your composer.json:

```
"repositories": [
    {
        "type": "git",
        "url": "https://bitbucket.org/terrasoff/phalcon-specifications"
    }
]
```


## And run composer command

`composer require terrasoff/phalcon-specifications`

# Usage

## Register search service 

Find [search service](src/ModelSearchService.php) defined in library and register it in service container.

```php
$di->set('modelsSearchManager', function () {
    return new terrasoff\Phalcon\Specifications\ModelSearchService($di->getShared('modelsManager'));
}
```

## Write custom search specification

You can find here an example of [robots search specification](examples/RobotsSpecification.php).

## Operations

Now we are ready to perform operations:

```php
$total = $service->count(Robot::class, new RobotsSpecification());

$firstRobot = $service->matchFirst(Robot::class, new RobotsSpecification());

// you can set up your search
$robots = $service->matchResults(
    Robot::class,
    (new RobotsSpecification())
        ->filterByName('bender')
        ->filterByProductionYear(2000)
);

// you can mix different s
$activeRobots = $service->matchResults(
    Robot::class,
    [
        Spec::true('is_active'),
        (new RobotsSpecification())
            ->filterByName('bender')
            ->filterByProductionYear(2000),
    ]
);

// you can paginate models
$robots = $service->paginate(Robot::class, new RobotsSpecification(), 10, 100);
```

# TODO

I've started to write this library while was working on some project with phalcon. 
Then i moved to another project and don't use phalcon anymore. 
But i hope someone will find this library useful and will do it better.
Would be great to see:

- tables' aliases
- join specification
- having specification
- greater/lower than
- ...