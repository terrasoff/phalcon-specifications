<?php

namespace terrasoff\Phalcon\Specifications\Helpers;

class SpecificationsHelper
{
    /**
     * Генерируем уникальное имя для параметра SQL запроса
     * @param string $parameterName
     * @return string
     */
    public static function getUniqueBindName(string $parameterName): string
    {
        return uniqid($parameterName);
    }
}
