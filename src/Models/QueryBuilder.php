<?php

namespace terrasoff\Phalcon\Specifications\Models;

use Phalcon\Mvc\Model\Query\Builder;

/**
 * Из нативного Builder не достучаться до некоторых свойств
 */
class QueryBuilder extends Builder
{
    /**
     * @return array
     */
    public function getBindParameters(): array
    {
        return $this->_bindParams ?? [];
    }

    /**
     * @return string|null
     */
    public function getConditions()
    {
        return $this->_conditions;
    }
}
