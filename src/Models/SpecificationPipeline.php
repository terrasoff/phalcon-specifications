<?php

namespace terrasoff\Phalcon\Specifications\Models;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use terrasoff\Phalcon\Specifications\Specifications\BaseSpecification;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Конвейер для построения коллекции спецификаций
 * и построения критерия поиска на основе спецификаций
 */
class SpecificationPipeline
{
    /**
     * @var BuilderInterface
     */
    private $builder;
    /**
     * Коллекция спецификаций
     *
     * @var SearchSpecificationInterface[]
     */
    private $specifications = [];

    public function __construct(BuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Добавление спецификации в коллекцию
     *
     * @param SearchSpecificationInterface $specification
     * @return $this
     */
    public function add(SearchSpecificationInterface $specification)
    {
        $this->specifications[] = $specification;

        return $this;
    }

    /**
     * Переопределение коллекции спецификаций
     *
     * @param SearchSpecificationInterface[] $specifications
     * @return $this
     */
    public function setSpecifications(array $specifications)
    {
        $this->specifications = $specifications;

        return $this;
    }

    /**
     * Применение всей коллекции спецификаций
     */
    public function apply()
    {
        foreach ($this->specifications as $specification) {
            $specification->apply($this->builder);
        }
    }
}
