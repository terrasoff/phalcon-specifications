<?php namespace terrasoff\Phalcon\Specifications\Interfaces;

use Phalcon\Mvc\Model\Query\BuilderInterface;

interface SearchSpecificationInterface
{
    /**
     * @param BuilderInterface $builder
     * @return BuilderInterface
     */
    public function apply(BuilderInterface $builder);
}
