<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Проверить значение атрибута на NULL
 */
class IsNullSpecification implements SearchSpecificationInterface
{
    /**
     * @var string
     */
    private $attributeName;
    /**
     * @var bool
     */
    private $isNull = true;

    /**
     * @param string $attributeName
     */
    public function __construct(
        string $attributeName
    ) {
        $this->attributeName = $attributeName;
    }

    /**
     * @return $this
     */
    public function reverse()
    {
        $this->isNull = false;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        $condition = $this->isNull ? 'IS NULL' : 'IS NOT NULL';

        $builder->andWhere("{$this->attributeName} $condition");
    }
}
