<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Helpers\SpecificationsHelper;
use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class DateRangeSpecification implements SearchSpecificationInterface
{
    /**
     * @var string
     */
    private $from;
    /**
     * @var string
     */
    private $to;
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @param string $attributeName
     */
    public function __construct(string $attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * @param string $from
     * @return $this
     */
    public function setFrom(string $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @param string $to
     * @return $this
     */
    public function setTo(string $to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @param array $range
     * @return $this
     */
    public function setRange(array $range)
    {
        $this->from = $range[0] ?? null;
        $this->to   = $range[1] ?? null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        $paramName = SpecificationsHelper::getUniqueBindName($this->attributeName);
        if (!empty($this->from)) {
            $bindName = "{$paramName}_from";
            $builder->andWhere("{$this->attributeName} >= :{$bindName}:", [
                $bindName => $this->from
            ]);
        }

        if (!empty($this->to)) {
            $bindName = "{$paramName}_to";
            $builder->andWhere("{$this->attributeName} <= :{$bindName}:", [
                $bindName => $this->to
            ]);
        }
    }
}
