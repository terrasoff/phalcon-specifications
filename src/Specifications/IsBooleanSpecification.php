<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

abstract class IsBooleanSpecification implements SearchSpecificationInterface
{
    /**
     * @var bool
     */
    private $isNullable = false;
    /**
     * @var string
     */
    private $attributeName;

    /**
     * @param string $attributeName
     */
    public function __construct(
        string $attributeName
    ) {
        $this->attributeName = $attributeName;
    }

    /**
     * @return $this
     */
    public function setNullable()
    {
        $this->isNullable = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        $condition = "{$this->attributeName} = {$this->getValue()}";
        if ($this->isNullable) {
            $condition .= " OR {$this->attributeName} IS NULL";
        }

        $builder->andWhere($condition);
    }

    /**
     * @return int
     */
    abstract protected function getValue(): int;
}
