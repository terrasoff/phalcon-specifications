<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Проверить значение атрибута на вхождение в множество
 */
class InSpecification implements SearchSpecificationInterface
{
    /**
     * @var string
     */
    private $attributeName;
    /**
     * @var array
     */
    private $value = [];

    /**
     * @param string $attributeName
     */
    public function __construct(
        string $attributeName,
        array $value
    ) {
        $this->attributeName = $attributeName;
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        if (!empty($this->value)) {
            $builder->inWhere($this->attributeName, $this->value);
        }
    }
}
