<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class GroupBySpecification implements SearchSpecificationInterface
{
    /**
     * @var array
     */
    private $groups;

    /**
     * @param array $groups
     */
    public function __construct(array $groups)
    {
        $this->groups = $groups;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        $builder->groupBy(
            implode(',', $this->groups)
        );
    }
}
