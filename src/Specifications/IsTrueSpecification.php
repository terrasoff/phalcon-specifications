<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

class IsTrueSpecification extends IsBooleanSpecification
{
    /**
     * {@inheritdoc}
     */
    protected function getValue(): int
    {
        return 1;
    }
}
