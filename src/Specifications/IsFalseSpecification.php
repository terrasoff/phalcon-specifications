<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

class IsFalseSpecification extends IsBooleanSpecification
{
    /**
     * {@inheritdoc}
     */
    protected function getValue(): int
    {
        return 0;
    }
}
