<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use Phalcon\Mvc\Model\Query\BuilderInterface;
use terrasoff\Phalcon\Specifications\Models\SpecificationPipeline;

/**
 * Фабрика для более лаконичной записи спецификаций
 */
class Spec
{
    public static function apply(
        BuilderInterface $builder,
        $specifications
    ) {
        $specifications = (array) $specifications;

        $pipeline = (new SpecificationPipeline($builder));
        foreach ($specifications as $specification) {
            $pipeline->add($specification);
        }

        $pipeline->apply();
    }

    public static function select()
    {
        return new SelectSpecification(... func_get_args());
    }

    public static function group()
    {
        return new GroupBySpecification(... func_get_args());
    }

    public static function or()
    {
        return new OrSpecification(... func_get_args());
    }

    public static function equal()
    {
        return new EqualSpecification(... func_get_args());
    }

    public static function null()
    {
        return new IsNullSpecification(... func_get_args());
    }

    public static function in()
    {
        return new InSpecification(... func_get_args());
    }

    public static function false()
    {
        return new IsFalseSpecification(... func_get_args());
    }

    public static function true()
    {
        return new IsTrueSpecification(... func_get_args());
    }

    public static function dateRange()
    {
        return new DateRangeSpecification(... func_get_args());
    }

    public static function search()
    {
        return new TextSearchSpecification(... func_get_args());
    }

    public static function sort()
    {
        return new SortSpecification(... func_get_args());
    }
}