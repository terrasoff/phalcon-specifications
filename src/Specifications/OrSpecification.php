<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Проверить значение атрибута на равенство
 */
class OrSpecification implements SearchSpecificationInterface
{
    /**
     * @var SearchSpecificationInterface[]
     */
    private $specs = [];

    /**
     * @param string $attributeName
     */
    public function __construct(
        ... $specs
    ) {
        $this->specs = $specs;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        $orBuilder = new QueryBuilder();
        foreach ($this->specs as $spec) {
            $loopBuilder = new QueryBuilder();
            $spec->apply($loopBuilder);
            $orBuilder->orWhere(
                $loopBuilder->getWhere(),
                $loopBuilder->getBindParameters()
            );
        }

        $where = $orBuilder->getWhere();
        $builder->andWhere(
            $where,
            $orBuilder->getBindParameters()
        );
    }
}
