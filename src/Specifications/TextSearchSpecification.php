<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use Phalcon\Mvc\Model\Query\BuilderInterface;
use terrasoff\Phalcon\Specifications\Helpers\SpecificationsHelper;
use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;

class TextSearchSpecification implements SearchSpecificationInterface
{
    /**
     * @var string
     */
    private $value;
    /**
     * @var string
     */
    private $field;

    public function __construct(
        string $fields,
        string $value = null
    ) {
        $this->field = $fields;
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        if (!empty($this->value)) {
            $bindName = SpecificationsHelper::getUniqueBindName($this->field);

            $builder->andWhere(
                "{$this->field} LIKE :{$bindName}:",
                [
                    $bindName => "%{$this->value}%"
                ]
            );
        }
    }
}
