<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

class SortSpecification implements SearchSpecificationInterface
{
    /**
     * @var array
     */
    private $sort;

    /**
     * @param array $sort
     */
    public function __construct(array $sort)
    {
        $this->sort = $sort;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        if (!empty($this->sort)) {
            $criteria = [];
            foreach ($this->sort as $field => $direction) {
                $criteria[] = "{$field} {$direction}";
            }
            $builder->orderBy(implode(',', $criteria));
        }
    }
}
