<?php

namespace terrasoff\Phalcon\Specifications\Specifications;

use terrasoff\Phalcon\Specifications\Helpers\SpecificationsHelper;
use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use Phalcon\Mvc\Model\Query\BuilderInterface;

/**
 * Проверить значение атрибута на равенство
 */
class EqualSpecification implements SearchSpecificationInterface
{
    /**
     * @var string
     */
    private $attributeName;
    /**
     * @var mixed
     */
    private $value;

    /**
     * @param string $attributeName
     */
    public function __construct(
        string $attributeName,
        $value
    ) {
        $this->attributeName = $attributeName;
        $this->value = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        if (!empty($this->value)) {
            $paramName = SpecificationsHelper::getUniqueBindName($this->attributeName);
            $builder->andWhere(
                "{$this->attributeName} = :{$paramName}:",
                [$paramName => $this->value]
            );
        }
    }
}
