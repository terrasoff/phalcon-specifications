<?php

namespace terrasoff\Phalcon\Specifications;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Manager;
use Phalcon\Mvc\Model\Query\BuilderInterface;
use Phalcon\Mvc\Model\Resultset\Simple;
use Phalcon\Paginator\Adapter\QueryBuilder;
use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class ModelSearchService
{
    /**
     * @var Manager
     */
    private $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $modelClass
     * @return BuilderInterface
     */
    private function getModelQueryBuilder(string $modelClass): BuilderInterface
    {
        return $this->manager
            ->createBuilder()
            ->from(
                $this->normalizeModelClass($modelClass)
            );
    }

    /**
     * @param string $modelClass
     * @return string
     */
    private function normalizeModelClass(string $modelClass): string
    {
        return ltrim($modelClass, '\\');
    }

    /**
     * @param string $modelClass
     * @param SearchSpecificationInterface[]|SearchSpecificationInterface $specifications
     * @return BuilderInterface
     */
    private function match(
        string $modelClass,
        $specifications
    ): BuilderInterface {
        $builder = $this->getModelQueryBuilder($modelClass);
        if (!is_array($specifications)) {
            $specifications = [$specifications];
        }

        Spec::apply($builder, $specifications);

        return $builder;
    }

    /**
     * @param string $modelClass
     * @param SearchSpecificationInterface[]|SearchSpecificationInterface $specifications
     * @param int $page
     * @param int $limit
     * @return QueryBuilder
     */
    public function paginate(
        string $modelClass,
        $specification,
        int $page,
        int $limit
    ): QueryBuilder
    {
        $builder = $this->match(
            $modelClass,
            $specification
        );

        // @link https://docs.phalconphp.com/en/3.4/db-pagination#adapters-usage
        return new QueryBuilder([
            'builder' => $builder,
            'page'    => $page,
            'limit'   => $limit
        ]);
    }

    /**
     * @param string $modelClass
     * @param SearchSpecificationInterface[]|SearchSpecificationInterface $specifications
     * @return BuilderInterface
     */
    public function matchResults(
        string $modelClass,
        $specifications
    ): Simple
    {
        return $this
            ->match($modelClass, $specifications)
            ->getQuery()
            ->execute();
    }

    /**
     * @param string $modelClass
     * @param SearchSpecificationInterface[]|SearchSpecificationInterface $specifications
     * @return BuilderInterface
     */
    public function count(
        string $modelClass,
        $specifications
    ): int
    {
        $builder = $this->getModelQueryBuilder($modelClass);

        if (!is_array($specifications)) {
            $specifications = [$specifications];
        }
        $builder->columns([
            'total' => 'COUNT(*)',
        ]);

        Spec::apply($builder, $specifications);

        $result = $builder->getQuery()->execute();

        return $result[0]->total;
    }

    /**
     * @param string $modelClass
     * @param $specifications
     * @return bool|Model
     */
    public function matchFirst(
        string $modelClass,
        $specifications
    ) {
        return $this
            ->matchResults($modelClass, $specifications)
            ->getFirst();
    }
}
