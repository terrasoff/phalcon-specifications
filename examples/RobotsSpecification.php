<?php

use Phalcon\Mvc\Model\Query\BuilderInterface;
use terrasoff\Phalcon\Specifications\Interfaces\SearchSpecificationInterface;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class RobotsSpecification implements SearchSpecificationInterface
{
    /**
     * @var
     */
    protected $productionYear;
    /**
     * @var string
     */
    private $name;

    /**
     * {@inheritdoc}
     */
    public function apply(BuilderInterface $builder)
    {
        Spec::apply($builder, [
           Spec::false('is_deleted'),
           Spec::search('name', $this->name),
           Spec::equal('production_year', $this->productionYear),
        ]);
    }

    /**
     * @param string $name
     * @return $this
     */
    public function filterByName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param int $productionYear
     * @return $this
     */
    public function filterByProductionYear(int $productionYear)
    {
        $this->productionYear = $productionYear;

        return $this;
    }
}
