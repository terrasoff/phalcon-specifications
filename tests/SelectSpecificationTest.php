<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class SelectSpecificationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testSelect(array $columns, string $expected)
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::select($columns);
        $spec->apply($builder);

        $this->assertEquals(
            $expected,
            strtolower($builder->getColumns())
        );
    }

    public function dataProvider(): array
    {
        return [
            [['a'], 'a'],
            [['a', 'b'], 'a,b'],
        ];
    }
}
