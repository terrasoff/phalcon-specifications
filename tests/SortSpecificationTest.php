<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class SortSpecificationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testSort(array $groups, string $expected)
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::sort($groups);
        $spec->apply($builder);

        $this->assertEquals(
            $expected,
            strtolower($builder->getOrderBy())
        );
    }

    public function dataProvider(): array
    {
        return [
            [['a' => 'asc'], 'a asc'],
            [['a' => 'asc', 'b' => 'desc'], 'a asc,b desc'],
        ];
    }
}
