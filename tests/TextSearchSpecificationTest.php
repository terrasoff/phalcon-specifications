<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class TextSearchSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testSearchSingleField()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::search('a', 'abc');
        $spec->apply($builder);
        $parameters = $builder->getBindParameters();
        $this->assertCount(1, $parameters);
        $this->assertEquals('%abc%', $parameters[key($parameters)]);
        $binds = array_keys($parameters);
        $paramName = array_shift($binds);

        $this->assertEquals(
            "a LIKE :{$paramName}:",
            $builder->getConditions()
        );
    }
}
