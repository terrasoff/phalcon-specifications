<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class InSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testIn()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::in('a', [1, 2]);

        $spec->apply($builder);
        $parameters = $builder->getBindParameters();
        $this->assertCount(2, $parameters);
        $param1 = array_search(1, $parameters);
        $param2 = array_search(2, $parameters);

        $this->assertEquals(
            "a IN (:{$param1}:, :{$param2}:)",
            $builder->getConditions()
        );
    }
}
