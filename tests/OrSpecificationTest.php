<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class OrSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testOrSpecification()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::or(
            Spec::equal('a', 1),
            Spec::equal('b', 2)
        );
        $builder->andWhere('c=3');

        $spec->apply($builder);
        $parameters = $builder->getBindParameters();
        $this->assertCount(2, $parameters);
        $paramA = array_search(1, $parameters);
        $paramB = array_search(2, $parameters);

        $this->assertEquals(
            "(c=3) AND ((a = :{$paramA}:) OR (b = :{$paramB}:))",
            $builder->getConditions()
        );
    }
}
