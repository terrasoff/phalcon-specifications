<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class IsTrueSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testTrue()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::true('a');
        $spec->apply($builder);

        $this->assertEquals(
            'a = 1',
            $builder->getConditions()
        );
    }

    public function testTrueOrNull()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::true('a')->setNullable();
        $spec->apply($builder);

        $this->assertEquals(
            'a = 1 OR a IS NULL',
            $builder->getConditions()
        );
    }
}
