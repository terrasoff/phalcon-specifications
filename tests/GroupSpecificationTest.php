<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class GroupSpecificationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testGroup(array $groups, string $expected)
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::group($groups);
        $spec->apply($builder);

        $this->assertEquals(
            $expected,
            strtolower($builder->getGroupBy())
        );
    }

    public function dataProvider(): array
    {
        return [
            [['a'], 'a'],
            [['a', 'b'], 'a,b'],
        ];
    }
}
