<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class NullSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testIsNull()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::null('a');
        $spec->apply($builder);

        $this->assertEquals(
            'a is null',
            mb_strtolower($builder->getConditions())
        );
    }

    public function testIsNotNull()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::null('a')->reverse();
        $spec->apply($builder);

        $this->assertEquals(
            "a is not null",
            mb_strtolower($builder->getConditions())
        );
    }
}
