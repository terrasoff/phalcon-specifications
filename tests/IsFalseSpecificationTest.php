<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class IsFalseSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testFalse()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::false('a');
        $spec->apply($builder);

        $this->assertEquals(
            'a = 0',
            $builder->getConditions()
        );
    }

    public function testFalseOrNull()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::false('a')->setNullable();
        $spec->apply($builder);

        $this->assertEquals(
            'a = 0 OR a IS NULL',
            $builder->getConditions()
        );
    }
}
