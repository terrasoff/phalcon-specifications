<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class EqualSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testEqual()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::equal('a', 1);

        $spec->apply($builder);
        $parameters = $builder->getBindParameters();
        $this->assertCount(1, $parameters);
        $param = array_search(1, $parameters);

        $this->assertEquals(
            "a = :{$param}:",
            $builder->getConditions()
        );
    }
}
