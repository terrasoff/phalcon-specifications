<?php

namespace App\Tests\unit\Specifications;

use terrasoff\Phalcon\Specifications\Models\QueryBuilder;
use terrasoff\Phalcon\Specifications\Specifications\Spec;

class DateRangeSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testEmptyDateRange()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::dateRange('a');
        $spec->apply($builder);

        $this->assertEquals(
            null,
            $builder->getConditions()
        );
    }

    public function testDateRange()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::dateRange('a')->setRange(['1','2']);
        $spec->apply($builder);

        $parameters = $builder->getBindParameters();
        $this->assertCount(2, $parameters);
        $param1 = array_search(1, $parameters);
        $param2 = array_search(2, $parameters);

        $this->assertEquals(
            "(a >= :{$param1}:) AND (a <= :{$param2}:)",
            $builder->getConditions()
        );
    }

    public function testDateRangeFrom()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::dateRange('a')->setFrom(1);
        $spec->apply($builder);

        $parameters = $builder->getBindParameters();
        $this->assertCount(1, $parameters);
        $param1 = array_search(1, $parameters);

        $this->assertEquals(
            "a >= :{$param1}:",
            $builder->getConditions()
        );
    }

    public function testDateRangeTo()
    {
        $builder = new QueryBuilder();
        $builder->from('table');

        $spec = Spec::dateRange('a')->setTo(1);
        $spec->apply($builder);

        $parameters = $builder->getBindParameters();
        $this->assertCount(1, $parameters);
        $param1 = array_search(1, $parameters);

        $this->assertEquals(
            "a <= :{$param1}:",
            $builder->getConditions()
        );
    }
}
